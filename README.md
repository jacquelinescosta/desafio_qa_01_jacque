![Alt text](inmetrics.png)

# **Desafio QA**

---

## Introdução

Automatizar os testes funcionais que são planejados durante a Sprint é umas das muitas atribuições do *Agile Tester*.

Hoje, é comum as aplicações serem desenvolvidas para diversas plataformas (Android, iOS, Website) e consumirem uma webservice para adicionar uma camada de segurança, redução de código, entre outros benefícios.

Deste modo, além de realizar testes de *front* em websites ou no app mobile, é importante adicionar testes de serviço para obter maior qualidade no produto desenvolvido.

---

## Objetivo

Avaliar as competências do candidato no entendimento e escrita de Cenários e na construção de testes automatizados, bem como no uso de boas práticas.

---

## Tecnologias

* *Para documentar os cenários especificados seguindo BDD*: **Cucumber**
* *Linguagem de programação*: **Ruby** ou **Java**
* *Bibliotecas*: as que achar necessário para execução dos testes

---

## Projeto
Criar um **único** projeto contendo:

* ***"Readme"*** com orientações de como instalar o ambiente e executar os testes
* A solução dos desafios propostos a seguir

---

## Desafio - WebSite
1. Fazer o cadastro de um novo ***Room*** no site [https://www.phptravels.net/supplier](https://www.phptravels.net/supplier). Para tal, preencher os campos das abas **General** e **Amenities**. Validar que o cadastro foi efetuado.
>* **usuário**: supplier@phptravels.com
>* **senha**: demosupplier

**Diferencial**: Utilizar conceito de PageObjects, geração dinâmica de dados e escolha aleatória na seleção das opções (combo boxes, check boxes, etc)

---

## Desafio - WebService
1. Enviar um **GET** para a API [http://swapi.co/api/films/](http://swapi.co/api/films/) e exibir o conteúdo do campo *“title”* de cada elemento da estrutura *“results”*. Validar o status code da resposta do serviço

**Diferencial**: Ao invés de todos os títulos, exibir somente dos filmes que tenham **George Lucas** como diretor e que **Rick McCallum** tenha participado como produtor

---

## Envio dos Desafios

Primeiramente, efetue um **fork** deste repositório. Desenvolva e faça os *commits* no seu **fork**. Quando terminar, envie um **Pull Request** através do **BitBucket**.

---

## Dica

Caso não consiga finalizar 100% do projeto, nos envie mesmo assim!

Nós avaliamos diversos itens como lógica, estrutura, padrões utilizados, entre outras coisas
