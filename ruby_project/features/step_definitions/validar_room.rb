include Test::Unit::Assertions
require_relative '../pages/page_room.rb'

Dado(/^que seja acessado o site https:\/\/www\.phptravels\.net\/supplier$/) do
  $browser=SiteElement.new("https://www.phptravels.net/supplier")
end

Quando(/^a página principal for apresentada$/) do
  $browser.login_username.send_keys('supplier@phptravels.com')
  $browser.login_password.send_keys('demosupplier')
  $body = $browser.validBody 
  $browser.submit_button.click
  
  wait = Selenium::WebDriver::Wait.new(:timeout => 10000)
  wait.until {$browser.validBody != $body}
  
end

Quando(/^acessar o menu "([^"]*)"$/) do |arg1|
  $browser.menu_Hotels.click
  $browser.menu_addRooms.click
end

Quando(/^informar os campos da aba "([^"]*)"$/) do |arg1|
  /preenchendo campos da aba General/

  option = Selenium::WebDriver::Support::Select.new($browser.field_status_addRooms)
  option.select_by(:value, 'No')

  $browser.field_room_addRooms.click
  $browser.field_roomlist_addRooms.click

  $browser.field_price_addRooms.send_keys "10"

end

Quando(/^clicar em "([^"]*)"$/) do |arg1|
  $browser.add_button.location_once_scrolled_into_view
  $browser.add_button.click
  sleep 10
end

Então(/^deverá ser exibida uma mensagem de erro informando que o campo é obrigatório$/) do
  $browser.error_message.location_once_scrolled_into_view
  assert_equal($browser.error_message.text, "The Hotel Name field is required.", "Não foi apresentada a mensagem de erro")
  
  $browser.close_browser
end

