# language: pt

Funcionalidade: Validações de fluxo de negócio referentes aos perfis de usuário
	Eu como usuário com determinado perfil de acesso não posso ou posso realizar determinadas operações no sistema


Cenário: Validar inserção de novo room

	Dado   que seja acessado o site https://www.phptravels.net/supplier
	Quando a página principal for apresentada
	E      acessar o menu "Hotels > Add Rooms"
    E      informar os campos da aba "General"
	E      informar os campos da aba "Amenities"
	E      clicar em "Submit"
	Então  deverá ser exibida uma mensagem de erro informando que o campo é obrigatório
