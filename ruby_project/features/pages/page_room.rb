require 'selenium-webdriver'
Selenium::WebDriver::Chrome.driver_path="/ruby_project/chromedriver"


class SiteElement

	def initialize(url)
		@driver=Selenium::WebDriver.for :chrome
		@driver.manage.window.maximize
		@driver.navigate.to url

		wait=Selenium::WebDriver::Wait.new(:timeout => 800)
  		element=@driver.find_element(name: 'email')
  		wait.until { element.displayed? }
	end

	def validBody()
		return @driver.find_element(:css, "body")
	end
	
	def login_username()
		return @driver.find_element(name: 'email')
	end
	
	def login_password()
		return @driver.find_element(name: 'password')
	end
	
	def menu_Hotels()
		return @driver.find_element(:xpath, "//a[@href='#Hotels']")
	end

	def menu_addRooms()
		return @driver.find_element(:xpath, "//a[contains(text(),'Add Room')]")
	end

	def field_status_addRooms()
		return @driver.find_element(name: "roomstatus")
	end

	def field_room_addRooms()
		return @driver.find_element(:xpath, "//span[@class='select2-chosen']")
	end

	def field_roomlist_addRooms()
		return @driver.find_element(:xpath, "//div[@class='select2-result-label']")
	end

	def field_price_addRooms()
		return @driver.find_element(name: "basicprice")
	end

	def submit_button()
		return @driver.find_element(:xpath, "//button[@type='submit']")
	end

	def add_button()
		return @driver.find_element(id: "add")
	end

	def error_message()
		return @driver.find_element(:xpath, "//div[@class='alert alert-danger']")
	end
	
	def close_browser()
		@driver.quit
	end
end 
