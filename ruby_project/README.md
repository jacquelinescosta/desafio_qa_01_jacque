
![Alt text](inmetrics.png)

# **Desafio QA - Solução**

---

##  Desafio 01

1. Sistema Operacional utilizado: Ubuntu versão 18.04
2. Linguagem: Ruby

### Itens necessários
 - Instalar o Ruby
	2. É possível verificar o procedimento explicado no seguinte post (pare antes do item *Installing the Gems*) no caso do linux: https://medium.com/@cristhian.fuertes/starting-a-ruby-project-with-cucumber-in-ubuntu-2944c5f3f5cd 
	3. O procedimento no windows é um pouco diferente, também existem alguns posts que descrevem como realizar a instalação, por exemplo (pare antes do item *Instalando o Bundler*): https://medium.com/qaninja/instalando-ruby-cucumber-e-capybara-no-windows-10-acb1fe833a95

Lembre-se de verificar se o ruby foi instalado corretamente pelo comando: ruby -v

- Obter dependências
Com o ruby instalado, é necessário obter as dependências do projeto, para isso vamos precisar instalar o Bundler.
Em um prompt basta executar: *gem install bundler*

- Em seguida, realizar o clone do repositório com o projeto. 
Acessar a pasta "\ruby_project", e executar o comando: *bundle install*
Neste momento as dependências listadas no arquivo "Gemfile" serão instaladas.

- No caso, existe uma dependência do teste com um item externo, como está sendo aberto o browser Chrome é necessário possuir o Driver dele. O Driver pode ser obtido em: http://chromedriver.chromium.org/downloads
Realize o download o Driver e coloque-o na pasta raiz do projeto ("\ruby_project"). 
Em seguida é necessário alterar o arquivo: *page_room.rb* informando o novo caminho do Chrome Driver (*.driver_path={caminho_do_driver}*)
---

## Execução do Teste

A execução do teste pode ser realizado via comando: *cucumber* na pasta "\ruby_project" 

---

## *Observações aos avaliadores*

- Este foi meu primeiro projeto de automação para avaliação de outro profissional
- Sei que alguns códigos podem ser melhorados, como o uso de tabela de dados no cucumber e também não deixar o utilizador alterar na mão arquivos (como no caso do Chrome Driver).
- Por alguma razão o usuário descrito no readme não tinha acesso para criar um novo hotel, então acabou que não consegui validar o cadastro como um todo, mas fica o exemplo de como faria este mapeamento.


Muito Obrigada!
Jacqueline