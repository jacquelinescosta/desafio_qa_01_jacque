package br.com.test.APIStarWars;

import static io.restassured.RestAssured.given;

import java.util.List;

import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static org.hamcrest.Matchers.*;


public class TestApiStarWars {
	public static String URI = "https://swapi.co/api/films/";

	@Test
	public void retornaFilmesGeorgeLucas() {
		
		RestAssured.defaultParser = Parser.JSON;
 
		Response resposta = 
		given()
				.headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
		.when()
				.get(URI)
		.then()
			   .statusCode(200)
			   .assertThat()
			   .body("results.director", hasItem("George Lucas"))
			   .body("results.producer", hasItem("Rick McCallum"))
			   .extract().response();
		
		System.out.print(resposta.jsonPath().get("results.title"));
	}

}
