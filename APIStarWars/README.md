
![Alt text](inmetrics.png)

# **Desafio QA - Solução**

---

##  Desafio 02

1. Sistema Operacional utilizado: Windows versão 10
2. Linguagem: Java / Rest-Assured

### Itens necessários
 - Basta importar o projeto como "existing maven projects" em alguma IDE (no caso utilizei o eclipse).

---

## Execução do Teste

A execução do teste pode ser realizada via Junit na classe "TestAPiStarWars.java"

---

## *Observações aos avaliadores*

- Sei que o hasItem só valida se existe aquele valor, mas não exatamente vai retornar somente ele. Infelizmente não consegui fazer esta parte.
- Sei que tem como fazer um assert dos valores retornados no responsebody (no caso daria para validar se os titulos retornados foram os esperados).


Muito Obrigada!
Jacqueline